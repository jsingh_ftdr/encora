package encoraSol

import (
	"encoding/json"
	"fmt"
	"log"
)

func EncoraSolution1() {
	for i, example := range examples {
		result, err := parse(example)
		if err != nil {
			panic(err)
		}
		j, err := json.MarshalIndent(result, " ", " ")
		if err != nil {
			panic(err)
		}
		log.Printf("Example %d: %s - %s", i, example, string(j))
	}
}

/*
Parse the text strings into a tree of node structs, this test is relevant because
work with a number of financial formats which require building processors.
https://play.golang.org/p/0ZvIlDBnTd
The answer to the first example should come out something like below, but
you can adjust the output to suit your answer/make it better.
*/

type node struct {
	Name     string  `json:"name"`
	Children []*node `json:"children,omitempty"`
}

var examples = []string{
	"[a,b,c]",
	"[a[aa[aaa],ab,ac],b,c[ca,cb,cc[cca]]]",
}

// ======================================================

func parse(v string) (*node, error) {
	root := &node{}

	// Parse here
	root = parseStringToNodes(v)

	return root, nil
}

type NodeLevel struct {
	Level  int
	Parent string
	Name   string
}

// function to parse the string and convert into nodes.
func parseStringToNodes(s string) *node {

	// Validate the String to be a valid Slice when parsed
	if !isValidSlice(s) {
		fmt.Println("String Slice is not Valid")
		return nil
	}

	// nodeLevels used to parse the string to hierarchy levels along with parent
	var nodeLevels []NodeLevel
	nodeLevels = append(nodeLevels, NodeLevel{0, "", "root"})

	level := 1
	lastLevel := 0
	parentStr := make(map[int]string)
	parentStr[1] = "root"
	childStr := ""

	// triming the outer array to extract the data. Will use this data as base.
	str := s[1:(len(s) - 1)]

	for _, v := range str {
		strVal := string(v) //convert rune to string

		if strVal == "[" { // "[" will mean that the next upcoming element is a child of current element
			level++
			parentStr[level] = childStr
		} else if strVal == "]" { // "]" will mean that the next upcoming element is a higher level element
			level--
		} else if strVal == "," { // "," mean next element is a new element under the same parent. lastLevel variable will control if the same word is element or we have a new element.
			lastLevel = level - 1
		} else {
			if lastLevel == level { // lastLevel matching with level means the current character is part of the running element
				childStr = childStr + strVal
				newNode := nodeLevels[len(nodeLevels)-1]
				newNode.Name = childStr
				nodeLevels[len(nodeLevels)-1] = newNode
			} else { // lastLevel not matching with level means the current character is part of a new element
				childStr = strVal
				nodeLevels = append(nodeLevels, NodeLevel{
					Level:  level,
					Parent: parentStr[level],
					Name:   strVal,
				})
			}
			lastLevel = level
		}
	}

	// Calling writeNodes to convert the node level hierarchy to actual hierarchy
	root := writeNodes(nodeLevels)

	return root
}

// writeNodes converts the node level hierarchy to actual hierarchy
func writeNodes(nodeLevels []NodeLevel) *node {

	nodes := []*node{}
	for _, n := range nodeLevels {
		currentNode := &node{Name: n.Name}
		nodes = append(nodes, currentNode)

		// attaching the current node to the parent node
		if n.Parent != "" {
			index := findParentNode(nodeLevels, n.Parent)
			if nodes[index].Children == nil || len(nodes[index].Children) == 0 {
				nodes[index].Children = []*node{currentNode}
			} else {
				nodes[index].Children = append(nodes[index].Children, currentNode)
			}
		}
	}

	// return the top most node
	return nodes[0]

}

// findParentNode: find the index of the parent in the nodeLevel slice,
// since both nodeLevel slice and node slice share the same running index.
func findParentNode(nodeLevels []NodeLevel, parentStr string) int {
	for i, n := range nodeLevels {
		if n.Name == parentStr {
			return i
		}
	}
	return -1
}

// function to validate the slice
func isValidSlice(s string) bool {
	pattern := make([]string, 0)
	validPattern := true
	index := 0
	pattern = append(pattern, "-")
	for _, v := range s {
		chr := string(v)
		if chr != "[" && chr != "]" {
			continue
		}
		if closingSlice(chr) {
			if matchingSlice(pattern[index], chr) {
				pattern = removeEntry(pattern, index)
				index = index - 1
			} else {
				validPattern = false
				break
			}
		} else {
			pattern = append(pattern, chr)
			index++
		}
	}
	if len(pattern) != 1 {
		return false
	}
	return validPattern
}

func removeEntry(s []string, i int) []string {
	copy(s[i:], s[i+1:])
	return s[:len(s)-1]
}

func closingSlice(s string) bool {
	if s == "]" {
		return true
	}
	return false
}

func matchingSlice(s1 string, s2 string) bool {
	if s1 == "[" && s2 == "]" {
		return true
	}
	return false
}
