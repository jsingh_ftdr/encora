package encoraSol

import (
	"log"
	"sync"
	"time"
)

func EncoraSolution2() {

	var wq waitQueue

	for v := 0; v < 10; v++ {
		wq.add()
		wq.wg.Add(1)
		go func(v int) {
			doublev := callDouble(v, &wq)
			log.Printf("Thread %d returned: %d", v, doublev)
		}(v)
	}

	time.Sleep(time.Second * 10)
}


type waitQueue struct {
	seq int
	wg  sync.WaitGroup
}

func (w *waitQueue) add() {
	w.seq = w.seq + 1
	if w.seq > 5 {
		w.wg.Wait()
	}

}

func (w *waitQueue) done() {
	w.seq = w.seq - 1
	w.wg.Done()
}

func callDouble(v int, wq *waitQueue) int {
	// Adjust code to call double only up to 5 times concurrently
	return double(v, wq)
}

func double(v int, wq *waitQueue) int {
	time.Sleep(time.Second)
	wq.done()
	return v * 2
}
